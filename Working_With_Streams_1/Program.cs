﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Working_With_Streams_1
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"E:\ayyo\created_files";
            string file_name_1 = "";
            Console.WriteLine("enter a file name, something like hello.txt or hello");
            file_name_1 = Console.ReadLine();

            //create a file and fill it with some data
            create_file_1(path, file_name_1);

            //lets read the file and display its contents
            read_file_1(path, file_name_1);

            Console.WriteLine("enter a file name for compression activities, something like hello.txt or hello");
            file_name_1 = Console.ReadLine();
            //lets create another file and fill it with some data
            //then compress it.
            compress_file_1(path, file_name_1);

            Console.WriteLine("That's all folks");
            Console.WriteLine("To be or not to be...not to be! --insert evil laugh--");
            Console.ReadLine();
            
        }

        private static void compress_file_1(string path, string file_name_1)
        {
            //first step is to get the file name with the proper extension
            string full_file_name = "";
            string compressed_full_file_name = "";
            string uncompressed_full_file_name = "uncompressed";
            if (file_name_1.Contains(".txt"))
            {
                full_file_name = file_name_1;

                //I have a slight problem here. if the file name entered by user is 'hello.txt', then I want the compressed
                //file ot have the name like 'hello_compressed.gz'
                //which means, i have to strip away the .txt so I can give the new file the .gz extentions
                //for that I use this for loop thingy.
                for(int i=0;i<file_name_1.Length;i++)
                {
                    if(file_name_1[i].Equals('.'))
                    {
                        break;//the idea here is to break off the .txt away from the rest of the name
                    }
                    compressed_full_file_name = compressed_full_file_name + file_name_1[i];
                }
                //here i finally have a file name with the .gz extension
                compressed_full_file_name = compressed_full_file_name + ".gz";
                //here the uncompressed file name with the .txt extension
                uncompressed_full_file_name = uncompressed_full_file_name + file_name_1;
            }
            else
            {
                full_file_name = file_name_1 + ".txt";
                compressed_full_file_name = file_name_1 + ".gz";
                uncompressed_full_file_name = uncompressed_full_file_name + full_file_name;
            }

            //lets get the full path of the file
            string full_path = Path.Combine(path, full_file_name);
            //lets get the full path of the compressed file
            string full_path_compressed = Path.Combine(path, compressed_full_file_name);
            //lets get the full path of the uncompressed file
            string full_path_uncompressed = Path.Combine(path, uncompressed_full_file_name);

            //i want to write into the file only if it is new
            //if it is already there, I wish to show an error message
            if (File.Exists(full_path) == false)
            {
                //now, create a file stream object and fill some data into the file
                using (FileStream stream_object = File.Create(full_path))
                {
                    //a byte stream array 
                    byte[] content_in_byte_stream;

                    //we are trying to look at the effect of compression on the stream
                    //so, lets generate a big stream which will have a big file size
                    //here, I am multiplying the byte equavalent of z 10000 times, and then returning an array
                    content_in_byte_stream = Enumerable.Repeat((byte)'z', 10000).ToArray();

                    //now that I have the byte stream version of the data to write, lets write it
                    //into the file 
                    //the first part is the stream that contains the data
                    //the second part is the offset, which indicates the spot from which we need to start writing
                    //the third part is the amount of bytes to write, in this case the entire length of it.
                    stream_object.Write(content_in_byte_stream, 0, content_in_byte_stream.Length);

                    //display success messages
                    Console.WriteLine("The file {0} is successfully created at {1} with content inserted into it", full_file_name, path);

                }

                //now, I need to do the exact same as above but this time a compressed file.
                //compression, works in two steps. 
                //first you have the file stream object which will point to the compressed file
                //second feed the file stream object to the compression stream object and let it do its thing

                using (FileStream stream_object = File.Create(full_path_compressed))
                {
                    //a byte stream array 
                    byte[] content_in_byte_stream;

                    //we are trying to look at the effect of compression on the stream
                    //so, lets generate a big stream which will have a big file size
                    //here, I am multiplying the byte equavalent of z 10000 times, and then returning an array
                    content_in_byte_stream = Enumerable.Repeat((byte)'z', 10000).ToArray();

                    //i have the stream, the exact same stream that was used to create the uncompressed file
                    //I have the file stream object. now, let me create a compressed file stream
                    using (GZipStream compression_object_stream = new GZipStream(stream_object, CompressionMode.Compress))
                    {
                        //now that i have the compression stream object which links to the file stream, I can write compressed data
                        compression_object_stream.Write(content_in_byte_stream, 0, content_in_byte_stream.Length);

                        //display success messages
                        Console.WriteLine("The compressed file {0} is successfully created at {1} with content inserted into it", compressed_full_file_name, path);
                    }

                }

                //at this point, we have the original file and also the compressed file. Lets find out there length
                //which is an indication of their file size. the compression results should be obvious

                FileInfo standard_file = new FileInfo(full_path);  //for the uncompressed file
                FileInfo compressed_file = new FileInfo(full_path_compressed);  //for the compressed file                    

                Console.WriteLine("The file size of uncompressed file {0} is {1}", full_path, standard_file.Length);
                Console.WriteLine("The file size of compressed file {0} is {1}", full_path_compressed, compressed_file.Length);

                //its time to uncompress the file

                //first we need a file stream which will hold the uncompressed file
                using (FileStream decompressed_stream_object = File.Create(full_path_uncompressed))
                {
                    //next the file stream that containts the compressed file
                    using (FileStream stream_object = File.OpenRead(full_path_compressed))
                    {
                        //at this stage I have the destination file stream which will hold the uncompressed file
                        //I also have the source file stream that contains the compressed file
                        //let me create a compression stream object that will do the actual decompression
                        using (GZipStream uncompress_gstream_object = new GZipStream(stream_object, CompressionMode.Decompress))
                        {
                            //uncompressed stream will be copied over to the destination stream 
                            uncompress_gstream_object.CopyTo(decompressed_stream_object);
                        }
                    }
                }

                //okay, so the decompression is done. lets output something.
                Console.WriteLine("Uncompressed file {0} is available at {1}", compressed_full_file_name, full_path_uncompressed);


            }
            else  //file already exists so giving the proper error message
            {
                Console.WriteLine("a file with name {0} already exists at {1} Cannot do compression", full_file_name, path);
            }
        }

        //this method will open the file in the path mentioned and display its contents
        private static void read_file_1(string path, string file_name_1)
        {
            //first step is to get the file name with the proper extension
            string full_file_name = "";
            if (file_name_1.Contains(".txt"))
            {
                full_file_name = file_name_1;
            }
            else
            {
                full_file_name = file_name_1 + ".txt";
            }

            //lets get the full path of the file
            string full_path = Path.Combine(path, full_file_name);

            //lets read the stuff only if the file exists
            if (File.Exists(full_path))
            {
                using (FileStream stream_object = File.OpenRead(full_path))
                {
                    //the file is a object. need to convert it into bytes

                    //first i need a byte array whom's length is equal to the length of the file contents
                    byte[] byte_of_stream = new byte[stream_object.Length];

                    //now, lets loop through the file stream, decode each item into its byte equivalent
                    for(int i=0;i<stream_object.Length;i++)
                    {
                        //convert each item in the stream object into a byte, and fill it into the byte array
                        byte_of_stream[i] = (byte)stream_object.ReadByte();
                    }

                    //at this point, all the data has been read from the file, and i can display it.
                    string read_data = Encoding.UTF8.GetString(byte_of_stream);

                    Console.WriteLine("the contents of the file are - {0}", read_data);
                }
            }
            else
            {
                Console.WriteLine("File {0} at {1} does not exist", full_file_name, path);
            }
        }

        //this method will create a file using  file streams
        private static void create_file_1(string path, string file_name_1)
        {
            //first step is to get the file name with the proper extension
            string full_file_name = "";
            if (file_name_1.Contains(".txt"))
            {
                full_file_name = file_name_1;
            }
            else
            {
                full_file_name = file_name_1 + ".txt";
            }

            //lets get the full path of the file
            string full_path = Path.Combine(path, full_file_name);

            //i want to write into the file only if it is new
            //if it is already there, I wish to show an error message
            if (File.Exists(full_path) == false)
            {
                //now, create a file stream object and fill some data into the file
                using (FileStream stream_object = File.Create(full_path))
                {
                    //getting the content to be filled into the file
                    string content_to_fill = "the known universe will always take care of you";

                    //now, this string is in object form. it needs to be converted into a stream of bytes
                    byte[] content_in_byte_stream = Encoding.UTF8.GetBytes(content_to_fill);

                    //now that I have the byte stream version of the data to write, lets write it
                    //into the file 
                    //the first part is the stream that contains the data
                    //the second part is the offset, which indicates the spot from which we need to start writing
                    //the third part is the amount of bytes to write, in this case the entire length of it.
                    stream_object.Write(content_in_byte_stream, 0, content_in_byte_stream.Length);

                    //display success messages
                    Console.WriteLine("The file {0} is successfully created at {1} with content inserted into it", full_file_name, path);
                   
                }
            }
            else  //file already exists so giving the proper error message
            {
                Console.WriteLine("a file with name {0} already exists at {1}", full_file_name, path);
            }


            
        }
    }
}










